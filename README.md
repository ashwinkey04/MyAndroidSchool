MyAndroidSchool
=================================
I will be pushing all the android applications I develop as a part of pursuing the Android starter course with Kotlin in Udacity and the Kotlin android fundamentals codelab

#### Shortcuts
* **Alt+Shift+C**: Recent changes
* **Shift x 2**: Search everywhere
* **Alt+Right(or Left)**: Switch between active tabs
* **Ctrl+O**: Override methods 